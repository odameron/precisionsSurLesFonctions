---
title: Précisions sur les fonctions
tags: programming, tutorial, python, java, function, argument
lang: fr
date: 2021-10-30
---

Précisions sur les fonctions

La version à jour de ce document est disponible à [https://gitlab.com/odameron/precisionsSurLesFonctions](https://gitlab.com/odameron/precisionsSurLesFonctions)


# À quoi servent les fonctions ?

Un algorithme est une méthode permettant de réaliser une tâche.
Cet algorithme est mis en oeuvre sous forme d'un programme.

La difficulté provient de l'écart entre la tâche à réaliser et les instructions dont dispose le langage de programmation.
En effet *la tâche est souvent compliquée* (sinon, on n'a pas vraiment besoin de faire un programme), alors que les *instructions sont élémentaires*.

L'approche classique consiste alors à composer l'algorithme en combinant plusieurs actions de haut niveau à l'aide des structures conditionnelles, des boucles, etc. (pensez à un logigramme).
Ces différentes actions de haut niveau seront elles-mêmes décomposées en sous-actions, et ainsi de suite.
Au fur et à mesure, on passe ainsi d'actions abstraites et générales à des sous-actions plus simples, jusqu'à atteindre les instructions élémentaires, qui sont concrètes et précises.

**Les fonctions répondent ainsi au besoin de désigner une suite d'instructions permettant de réaliser une action et de pouvoir exécuter cette action plusieurs fois**, éventuellement dans plusieurs situations.
Lors de la création de l'algorithme, on aura donc des fonctions de bas niveau composées de quelques instructions élémentaires, et graduellement des fonctions de niveau de plus en plus haut, qui vont nécessiter de plus en plus d'instructions agencées de façons de plus en plus complexes.
À chaque niveau, les fonctions pourront ainsi s'appuyer sur les fonctions des niveaux inférieurs.
Parmi les instructions à exécuter pour une fonction, on peut donc trouver des appels à d'autres fonctions (ou à la fonction elle-même, mais c'est plus risqué, on en reparlera à propos des fonctions récursives).





# Différence entre déclaration et appels

## Motivation

- une fonction permet de *donner un nom à une suite d'instructions* afin de *pouvoir exécuter ces instructions plusieurs fois* sans avoir à faire de copier-coller plus ou moins hasardeux
- on distingue donc :
    - la **déclaration d'une fonction** qui consiste à expliquer quelle suite d'instructions est associée à la fonction. 
    - les **appels à la fonction** qui consistent à exécuter la suite d'instructions associées à la fonction.


## principes
- la façon de désigner la suite d'instructions associée à une fonction doit être unique
    - en général ça se fait via le nom de la fonction (on ne peut pas avoir deux fonctions qui s'appellent pareil)
    - parfois deux fonctions peuvent avoir le même nom si on peut faire la différence entre elles selon leurs arguments
- il faut avoir déclaré la fonction pour pouvoir l'appeler
    - pour les langages de scripts (par exemple Python), il faut que la déclaration soit avant l'appel
    - pour les langages orientés-objets, il faut simplement que la déclaration existe ailleurs
- on ne déclare une fonction qu'une seule fois...
- ...mais on peut l'appeler autant de fois que l'on veut


TODO : expliquer qu'à la fin de l'exécution des instructions, on reprend de l'endroit où a eu lieu l'appel de la fonction.


## Déclaration d'une fonction

La **déclaration d'une fonction** consiste à la définir en donnant les instructions qui la composent.
À ce niveau, on donne juste la recette pour réaliser l'action souhaitée dans le cas général mais on ne fait rien. Les instructions ne seront executées que lorsque la fonction sera appelée.

En général, le nom de la fonction doit contenir un verbe d'action.

La structure de la déclaration dépend évidemment de chaque langage de programmation, mais on trouve toujours au moins :

- le nom de la fonction
- des arguments éventuels (cf. section suivante)
- les instructions qui constituent la fonction.


### Déclaration d'une fonction en Python

En Python, la déclaration d'une fonction est composée successivement de :

- mot-clé `def`
- nom de la fonction
- en général, pas d'espace
- parenthèse ouvrante
- une liste éventuellement vide de noms d'arguments, séparés par des virgules
- parenthèse fermante
- caractère ":"
- retour à la ligne
- le bloc des instructions qui consituent la fonction, indentées d'un niveau par rapport au mot-clé "def". la fin de l'indentation signale la fin du bloc d'instructions.

Exemple :

```python
def uneFonctionSansArguments():
    instruction_1
    instruction_2
    ...
    instruction_n # dernière instruction

def autreFonctionAvecUnArgument(nomArg1):
    instruction_1
    instruction_2
    ...
    instruction_m # dernière instruction

def encoreUneFonctionAvecPlusieursArguments(nomArg1, nomArg2, ..., nomArgk):
    instruction_1
    instruction_2
    ...
    instruction_p # dernière instruction
```


### Déclaration d'une fonction en Java

En Java, la déclaration d'une fonction est composée successivement de :

- 0 ou 1 indicateur de visibilité (mot-clé `public`, `protected`, `private`)
- mot-clé `static` s'il s'agit d'une méthode de classe
- type du résultat de la fonction (ou mot-clé `void` si elle ne renvoie rien)
- nom de la fonction
- en général, pas d'espace
- parenthèse ouvrante
- une liste éventuellement vide dont les éléments sont séparés par des virgules
    - type de l'argument
    - espace
    - nom de l'argument
- parenthèse fermante
- accolade ouvrante
- retour à la ligne
- le bloc des instructions qui consituent la fonction
- accolade fermante

Exemple :

```java
public static void uneFonctionSansArguments(){
    instruction_1;
    instruction_2;
    ...
    instruction_n;
}

public static int autreFonctionAvecUnArgument(typeArg1 nomArg1){
    instruction_1;
    instruction_2;
    ...
    instruction_m;
    return 42;
}

public static float encoreUneFonctionAvecPlusieursArguments(type1 nomArg1, type2 nomArg2, ..., typek nomArgk){
    instruction_1;
    instruction_2;
    ...
    instruction_p;
    return 3.14;
}
```


### Exemple concret : deux versions de la fonction saluer

La première fonction `saluer` se contente d'une salutation impersonnelle en affichant deux messages.
On imagine qu'elle est appelée à de nombreuses reprises dans un programme. 
Avoir recours à une fonction pour réaliser cette tâche présente deux avantages :

- elle simplifie l'écriture du code puisqu'on n'a pas besoin de saisir à chaque fois les deux affichages ;
- elle rend l'écriture du code plus modulaire et plus facilement évolutive puisque si on décide de changer de formule de salutation, il n'y a qu'à modifier le code de la fonction (donc en un seul endroit), et pas les différents endroits où cette fonction est appelée.

**TODO: * cf. `methode_variable-parametre.tex`

En Python :

```python
def saluer():
    ...
```

En Java :

```java
public static void saluer() {
    ...
}
```

Si on veut personnaliser la fonction de salutation, on peut appeler quelqu'un par son nom.

- remarquez que la définition de la fonction `saluerQuelqun` explique comment saluer quelqu'un par son nom sans que l'on connaisse le nom de la personne à saluer, et que cette explication est générale et marche quelque soit le nom de la personne.
- dans la définition de la fonction, on a besoin de faire référence à ce nom, en disant qu'il sera fourni lorsqu'on exécutera la fonction. On utilise donc pour cela un **argument** (cf. section suivante).

**TODO: * cf. `methode_variable-parametre.tex`

En Python :

```python
def saluerQuelqun(nom):
    ...
```

En Java :

```java
public static void saluer(String nom) {
    ...
}
```





## Appel d'une fonction et exécution de ses instructions

L'**appel d'une fonction** provoque l'exécution des instructions qui constituent la fonction.
Alors que la déclaration de la fonction donnait une recette dans le cas général, chaque appel de la fonction a lieu dans un contexte particulier (voir section sur les arguments).

Une fois que toutes les instructions de la fonction ont été exécutées, le programme reprend de là où la fonction avait été appelée.

Il faut bien comprendre que seul l'appel d'une fonction entraîne l'exécution de ses instructions.


### Chronologie de l'appel d'une fonction

On reprend l'exemple des fonctions `saluer` et `saluerQuelqun`.

**TODO: * cf. `methode_variable-parametre.tex`

En Python :

```python
print("Avant l'appel")
saluer()
print("Après l'appel")

saluerQuelqun("Riri")
saluerQuelqun("Fifi")
```

En java :

```java
System.out.println("Avant l'appel");
saluer();
System.out.println("Après l'appel");

saluerQuelqun("Riri");
saluerQuelqun("Fifi");
```

Le résultat sera :
```bash
Avant l'appel
Hello world
Have a nice day!
Après l'appel
Hello Riri
Have a nice day!
Hello Fifi
Have a nice day!
```

**Todo:** expliquer chronologie cf section 4.1 `methode_variable-parametre.pdf`


# Les arguments d'une fonction

## Motivation

- on peut parfois vouloir que l'execution des instructions dépende d'une ou de plusieurs éléments dont on ne connaît pas la valeur précise au moment de la déclaration de la fonction. On a alors recours à des **arguments** (TODO : lien vers section).

On a vu rapidement à la section précédente l'utilisation d'un argument avec la fonction `saluerQuelqun`.
Cette section revient plus en détail sur le mécanisme des arguments et ses subtilités.

**Todo:** cf sections 4.2 et 4.3 `methode_variable-parametre.pdf`


# La valeur renvoyée par une fonction : différence entre return et print


## Motivation

- on peut aussi vouloir récupérer une valeur résultant de l'exécution des instructions (TODO : lien vers section).
- `return 3.14` signifie "le résultat de la fonction est la valeur 3.14"
- l'appel d'une fonction provoque l'exécution de ses instructions, et s'il y a un `return`, l'appel est remplacé par la valeur de la fonction
- on peut alors faire de ce que l'on veut avec ce résultat (cf. ci-dessous) :
    - l'afficher directement
    - l'afficher de la façon que l'on veut et qui n'est pas prévue par la fonction
    - le réutiliser pour faire quelque chose d'autre


## Exemples d'utilisation du résultat d'une fonction

```python
# affichage direct du résultat d'une fonction
print(f(3))

# affichage personnalisé faisant intervenir le résultat d'une fonction
print("Le résultat de f pour 3 vaut: " + str(f(3)))

# réutilisation du résultat d'une fonction
# on veut mettre le double de f(3) dans maVariable
maVariable = 2 * f(3)
```

